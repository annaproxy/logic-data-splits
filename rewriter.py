"""
Tools for src file rewriting (where target file stays the same)
"""
from tqdm import tqdm
import random
import os
from typing import Optional, List
import random
from ltl_parser import (
    tokenize_formula,
    LTLFormulaLeaf,
    LTLFormulaBinaryOp,
    LTLFormulaUnaryOp,
    ParseError,
    Token,
    parse_polish_formula,
)

FOLDER_NAME = "new_format"


def edit_source_file(
    filename: List[str],
    function,
    addition_str: Optional[str] = None,
    addition_path: Optional[str] = None,
):
    assert ".src" in filename[-1] or ".src", "Must be a source filename"
    if addition_path is not None:
        assert addition_str is None, "Either a str or a path can be given"
        # Insert additional path between the folder and the final file
        out_file = os.path.join(
            FOLDER_NAME, *filename[:-1], addition_path, filename[-1]
        )
    else:
        out_file = os.path.join(
            FOLDER_NAME,
            *filename[:-1],
            filename[-1].replace(".src", f"_{addition_str}.src"),
        )
    if os.path.exists(out_file):
        raise FileExistsError("File " + out_file + " exists, quitting!")

    in_file = os.path.join(FOLDER_NAME, *filename)

    with open(in_file, "r") as f:
        lines = [s.strip() for s in f.readlines()]

    with open(out_file, "w") as f:
        for line in tqdm(lines, total=len(lines)):
            line = function(line)
            f.write(line + "\n")


def simple_apply(original_line: str, rewrite_func):
    return rewrite_func(original_line)


def apply_until(original_line: str, rewrite_func):
    """
    Iteratively apply the rewrite_func to the original_line until there is nothing to rewrite.
    """
    amt = 1
    amts = []
    new_formula = None
    while amt > 0:
        tokens = tokenize_formula(original_line.replace(" ", "").strip(), "network")
        new_formula, _, amt = rewrite_func(tokens)
        original_line = new_formula.to_str(format="network-polish", spacing=" ")
        amts.append(amt)
    return new_formula, amts


def switch_args_prob_balance(lines):
    """For creating a balanced tree dataset"""
    output = []
    for line in tqdm(lines, total=len(lines)):
        tokens = tokenize_formula(line.replace(" ", "").strip(), "network")
        new_formula, _ = parse_polish_build_switched_tokenlist(
            [*enumerate(tokens)], 0.5
        )
        new_line = new_formula.to_str(format="network-polish", spacing=" ")
        token_positions = new_formula.get_old_positions()
        output.append((new_line, token_positions))
    return output


def switch_tree(line: str):
    tokens = tokenize_formula(line.replace(" ", "").strip(), "network")
    new_formula, _ = parse_polish_build_switched_tokenlist([*enumerate(tokens)], 1)
    return new_formula.to_str(format="network-polish", spacing=" ")


def rewrite_negation(line: str):
    line = line.replace("! |", "nor")
    line = line.replace("! &", "nand")
    line = line.replace("! <->", "not<->")
    line = line.replace("! xor", "notxor")
    for letter in "abcde":
        line = line.replace(f"! {letter}", f"not{letter}")
    return line


def replace_not_b(line):
    return line.replace("! b", "! & b b")


def insert_double_negation(line: str):
    line = line.split()
    while random.random() < 0.2:
        pos = random.randint(0, len(line) - 1)
        line.insert(pos, "!")
        line.insert(pos, "!")
    return " ".join(line)


def detect_and_not_pattern_and_replace(token_list, replace=True, removed=0):
    """
    Replaces & ! A B  with & B XOR A B
    Returns:
        Tuple of (Rewritten LTLFormula, remaining token list, # of subexpressions found)
    """
    if len(token_list) == 0:
        raise ParseError("Attempt to parse from empty token list")
    num_children, type_, *name = token_list.pop(0)
    if num_children == 2:
        lchild, token_list, removed = detect_and_not_pattern_and_replace(
            token_list, replace, removed
        )
        rchild, token_list, removed = detect_and_not_pattern_and_replace(
            token_list, replace, removed
        )
        # ! A and B
        if type_ is Token.AND and (lchild.type_ is Token.NOT):
            assert not rchild.type_ is Token.NOT
            # A xor B
            xor = LTLFormulaBinaryOp(Token.XOR, lchild.child, rchild)
            return (
                # & A xor B B
                LTLFormulaBinaryOp(Token.AND, xor, rchild),
                token_list,
                removed + 1,
            )
        elif type_ is Token.AND and (rchild.type_ is Token.NOT):
            assert not lchild.type_ is Token.NOT
            xor = LTLFormulaBinaryOp(Token.XOR, lchild, rchild.child)
            return (
                LTLFormulaBinaryOp(Token.XOR, lchild, xor),
                token_list,
                removed + 1,
            )
        else:
            return LTLFormulaBinaryOp(type_, lchild, rchild), token_list, removed
    elif num_children == 1:
        child, token_list, removed = detect_and_not_pattern_and_replace(
            token_list, replace, removed
        )
        return LTLFormulaUnaryOp(type_, child), token_list, removed
    elif num_children == 0:
        if type_ == Token.AP:
            return LTLFormulaLeaf(type_, ap=name[0]), token_list, removed
        else:
            return LTLFormulaLeaf(type_, ap=None), token_list, removed
    else:
        raise ParseError("Illegal token '" + str(type_) + "'")


def parse_polish_build_switched_tokenlist(token_list, switch_prob):
    """
    Switches switch_prob * 100 percent of the trees
    Returns the formula where formula.old_pos is set for later analysis
    For reproducible results, random.seed should be set before applying to all formulas sequentially.
    """
    if len(token_list) == 0:
        raise ParseError("Attempt to parse from empty token list")
    old_pos, (num_children, type_, *name) = token_list.pop(0)

    if num_children == 2:
        lchild, token_list = parse_polish_build_switched_tokenlist(
            token_list, switch_prob
        )
        rchild, token_list = parse_polish_build_switched_tokenlist(
            token_list, switch_prob
        )
        # Switch
        if switch_prob == 1 or random.random() < switch_prob:
            tree = LTLFormulaBinaryOp(type_, rchild, lchild)
            tree.set_old_pos(old_pos)
            return tree, token_list
        else:
            tree = LTLFormulaBinaryOp(type_, lchild, rchild)
            tree.set_old_pos(old_pos)
            return tree, token_list
    elif num_children == 1:
        child, token_list = parse_polish_build_switched_tokenlist(
            token_list, switch_prob
        )
        tree = LTLFormulaUnaryOp(type_, child)
        tree.set_old_pos(old_pos)
        return tree, token_list
    elif num_children == 0:
        if type_ == Token.AP:
            tree = LTLFormulaLeaf(type_, ap=name[0])
            tree.set_old_pos(old_pos)
            return tree, token_list
        else:
            tree = LTLFormulaLeaf(type_, ap=None)
            tree.set_old_pos(old_pos)
            return tree, token_list


def parse_polish_replace_not_op_with_simpler(
    token_list, replace=True, op=Token.XOR, new_op=Token.EQUIV, removed=0
):
    """
    Parses a formula and replaces occurences of ! (... OP ... ) as follows:
        ! ( a OP b ) => a NEW_OP b

    Parameters:
        token_list: List of tokens as tokenized by ltl_parser
        replace: boolean, whether to replace occurences (True) or only count them (False)
        op: type of the binary operator to remove
        new_op: New op according to equivalence law

    Returns:
        Tuple of (Rewritten LTLFormula, remaining token list, # of subexpressions replaced)
    """
    if op is new_op:
        raise ValueError(f"op can't be same as new_op (both {op})")
    if len(token_list) == 0:
        raise ParseError("Attempt to parse from empty token list")
    num_children, type_, *name = token_list.pop(0)
    if num_children == 2:  # TODO parse_polish_replace_not_op_with_simpler
        lchild, token_list, removed = parse_polish_replace_not_op_with_simpler(
            token_list, replace, op, new_op, removed
        )
        rchild, token_list, removed = parse_polish_replace_not_op_with_simpler(
            token_list, replace, op, new_op, removed
        )
        return LTLFormulaBinaryOp(type_, lchild, rchild), token_list, removed
    elif num_children == 1:
        # Must be a NOT operator in the propositional case
        child, token_list, removed = parse_polish_replace_not_op_with_simpler(
            token_list, replace, op, new_op, removed
        )
        # If the negation is followed by the desired OP, rewrite it
        if child.type_ is op:
            if replace:
                return (
                    LTLFormulaBinaryOp(new_op, child.lchild, child.rchild),
                    token_list,
                    removed + 1,
                )
            else:
                return LTLFormulaUnaryOp(type_, child), token_list, removed + 1
        else:
            return LTLFormulaUnaryOp(type_, child), token_list, removed
    elif num_children == 0:
        if type_ == Token.AP:
            return LTLFormulaLeaf(type_, ap=name[0]), token_list, removed
        else:
            return LTLFormulaLeaf(type_, ap=None), token_list, removed
    else:
        raise ParseError("Illegal token '" + str(type_) + "'")


def parse_polish_replace_and_leftnot_with_and_rightnot(
    token_list, replace=True, removed=0
):
    """This function should be given the rewritten dataset where not both children have NOT"""
    if len(token_list) == 0:
        raise ParseError("Attempt to parse from empty token list")
    num_children, type_, *name = token_list.pop(0)
    if num_children == 2:
        (
            lchild,
            token_list,
            removed,
        ) = parse_polish_replace_and_leftnot_with_and_rightnot(
            token_list, replace, removed
        )
        (
            rchild,
            token_list,
            removed,
        ) = parse_polish_replace_and_leftnot_with_and_rightnot(
            token_list, replace, removed
        )
        # If type is and and left child starts with not, move left child to right.
        if type_ is Token.AND and lchild.type_ is Token.NOT:
            assert not (rchild.type_ is Token.NOT), "Wrong dataset motherfucker"
            return LTLFormulaBinaryOp(type_, rchild, lchild), token_list, removed
        else:
            return LTLFormulaBinaryOp(type_, lchild, rchild), token_list, removed

    elif num_children == 1:
        child, token_list, removed = parse_polish_replace_and_leftnot_with_and_rightnot(
            token_list, replace, removed
        )
        return LTLFormulaUnaryOp(type_, child), token_list, removed
    elif num_children == 0:
        if type_ == Token.AP:
            return LTLFormulaLeaf(type_, ap=name[0]), token_list, removed
        else:
            return LTLFormulaLeaf(type_, ap=None), token_list, removed
    else:
        raise ParseError("Illegal token '" + str(type_) + "'")


def parse_polish_remove_not_op(
    token_list, replace=True, op=Token.AND, new_op=Token.OR, removed=0
):
    """
    Parses a formula and replaces occurences of ! (... OP ... ) as follows:
        ! ( a OP b ) => !a NEW_OP !b

    Parameters:
        token_list: List of tokens as tokenized by ltl_parser
        replace: boolean, whether to replace occurences (True) or only count them (False)
        op: type of the binary operator to remove
        new_op: New op according to demorgans law

    Returns:
        Tuple of (Rewritten LTLFormula, remaining token list, # of subexpressions replaced)
    """
    if op is new_op:
        raise ValueError(f"op can't be same as new_op (both {op})")
    if len(token_list) == 0:
        raise ParseError("Attempt to parse from empty token list")
    num_children, type_, *name = token_list.pop(0)
    if num_children == 2:
        lchild, token_list, removed = parse_polish_remove_not_op(
            token_list, replace, op, new_op, removed
        )
        rchild, token_list, removed = parse_polish_remove_not_op(
            token_list, replace, op, new_op, removed
        )
        return LTLFormulaBinaryOp(type_, lchild, rchild), token_list, removed
    elif num_children == 1:
        # Must be a NOT operator in the propositional case
        child, token_list, removed = parse_polish_remove_not_op(
            token_list, replace, op, new_op, removed
        )

        # If the negation is followed by the desired OP, rewrite it
        if child.type_ is op:
            if replace:
                neg_lchild = LTLFormulaUnaryOp(Token.NOT, child.lchild)
                neg_rchild = LTLFormulaUnaryOp(Token.NOT, child.rchild)
                return (
                    LTLFormulaBinaryOp(new_op, neg_lchild, neg_rchild),
                    token_list,
                    removed + 1,
                )
            else:
                return LTLFormulaUnaryOp(type_, child), token_list, removed + 1
        else:
            return LTLFormulaUnaryOp(type_, child), token_list, removed
    elif num_children == 0:
        if type_ == Token.AP:
            return LTLFormulaLeaf(type_, ap=name[0]), token_list, removed
        else:
            return LTLFormulaLeaf(type_, ap=None), token_list, removed
    else:
        raise ParseError("Illegal token '" + str(type_) + "'")


def parse_polish_remove_not_op_not(
    token_list, replace=True, op=Token.AND, new_op=Token.OR, removed=0
):
    """
    Parses a formula but removes all occurences of ! A OP ! B as follows:
        !a OP !b => ! ( a NEW_OP b )

    Parameters:
        token_list: List of tokens as tokenized by ltl_parser
        replace: boolean, whether to replace occurences (True) or only count them (False)
        op: type of the binary operator to remove
        new_op: New op according to demorgans law

    Returns:
        Tuple of (Rewritten LTLFormula, remaining token list, # of subexpressions replaced)
    """
    if op is new_op:
        raise ValueError(f"op can't be same as new_op (both {op})")
    if len(token_list) == 0:
        raise ParseError("Attempt to parse from empty token list")
    num_children, type_, *name = token_list.pop(0)
    if num_children == 2:
        lchild, token_list, removed = parse_polish_remove_not_op_not(
            token_list, replace, op, new_op, removed
        )
        rchild, token_list, removed = parse_polish_remove_not_op_not(
            token_list, replace, op, new_op, removed
        )

        # If this is the desired op and both children are negated, rewrite
        if type_ is op and lchild.type_ is Token.NOT and rchild.type_ is Token.NOT:
            if replace:
                inner = LTLFormulaBinaryOp(new_op, lchild.child, rchild.child)
                return LTLFormulaUnaryOp(Token.NOT, inner), token_list, removed + 1
            else:
                return (
                    LTLFormulaBinaryOp(type_, lchild, rchild),
                    token_list,
                    removed + 1,
                )
        else:
            return LTLFormulaBinaryOp(type_, lchild, rchild), token_list, removed
    elif num_children == 1:
        child, token_list, removed = parse_polish_remove_not_op_not(
            token_list, replace, op, new_op, removed
        )
        return LTLFormulaUnaryOp(type_, child), token_list, removed
    elif num_children == 0:
        if type_ == Token.AP:
            return LTLFormulaLeaf(type_, ap=name[0]), token_list, removed
        else:
            return LTLFormulaLeaf(type_, ap=None), token_list, removed
    else:
        raise ParseError("Illegal token '" + str(type_) + "'")

def create_splits(filenames, simple=False):
    """
    Creates all possible rewriting splits.
    """
    for short_name, rewrite_func in [
        (
            "without_not_and",
            lambda x: parse_polish_remove_not_op(x, True, Token.AND, Token.OR),
        ),
        (
            "without_not_or",
            lambda x: parse_polish_remove_not_op(x, True, Token.OR, Token.AND),
        ),
        (
            "without_not_and_not",
            lambda x: parse_polish_remove_not_op_not(x, True, Token.AND, Token.OR),
        ),
        (
            "without_not_or_not",
            lambda x: parse_polish_remove_not_op_not(x, True, Token.OR, Token.AND),
        ),
        (
            "without_not_xor",
            lambda x: parse_polish_replace_not_op_with_simpler(
                x, True, Token.XOR, Token.EQUIV
            ),
        ),
        (
            "without_and_not_rewritten",
            lambda x: detect_and_not_pattern_and_replace(x, True),
        )(
            "without_not_b_rewritten",
            replace_not_b,
        )(
            "without_and_leftnot_rewritten",
            lambda x: parse_polish_replace_and_leftnot_with_and_rightnot(x, True),
        ),
    ]:

        def rewrite(line):
            new_formula, amt = apply_until(line, rewrite_func)
            # When done, replace any double negation
            return new_formula.to_str(format="network-polish", spacing=" ").replace(
                "! ! ", ""
            )

        def rewrite_simple(line):
            return rewrite_func(line)

        for filename in filenames:
            edit_source_file(
                filename,
                addition_str=short_name,
                function=rewrite if not simple else rewrite_simple,
            )


if __name__ == "__main__":
    # To create the files without certain patterns
    create_splits(
        [
            ["balanced/train.src"],
            ["balanced/val.src"],
            ["balanced/test.src"],
        ],
        simple=True,
    )
    create_splits(
        [
            ["balanced/train_without_not_and_not.src"],
            ["balanced/val_without_not_and_not.src"],
            ["balanced/test_without_not_and_not.src"],
        ],
        simple=False,
    )
