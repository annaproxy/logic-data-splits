"""
Input: original single formula files from deepltl repository (without spaces)
Output: file with spaces
"""
from ltl_parser import tokenize_formula, parse_polish_formula
from tqdm import tqdm


def rewrite(filename: str):
    with open(filename + ".txt", "r") as f:
        lines = [s.strip() for s in f.readlines()]
    src_lines = []
    tgt_lines = []
    for i, line in tqdm(enumerate(lines)):
        if i % 2 == 0:
            tokens = tokenize_formula(line, "network")
            formula, _ = parse_polish_formula(tokens)
            src_lines.append(formula.to_str(format="network-polish", spacing=" "))
        else:
            tgt_lines.append(" ".join(line))

    with open(filename + ".src", "w") as f:
        for l in src_lines:
            f.write(l + "\n")
    with open(filename + ".tgt", "w") as f:
        for l in tgt_lines:
            f.write(l + "\n")


if __name__ == "__main__":
    for s in [
        'train_no_not_and_no_double_negation.txt',
        'test_no_not_and_no_double_negation.txt',
        'val_no_not_and_no_double_negation.txt',
        'train_no_not_or_not_no_double_negation.txt',
        'val_no_not_or_not_no_double_negation.txt',
        'test_no_not_or_not_no_double_negation.txt',
    ]:
        rewrite(s)
