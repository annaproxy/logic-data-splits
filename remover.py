"""
Tools for removing src,tgt pairs from the dataset
"""
import os
import random

from tqdm import tqdm

FOLDER_NAME = "balanced"
# FOLDER_NAME = "new_format/balanced_include_unsat"

from ltl_parser import (
    tokenize_formula,
    parse_polish_formula,
    LTLFormulaLeaf,
    LTLFormulaBinaryOp,
    LTLFormulaUnaryOp,
    ParseError,
    Token,
)


def detect_pattern_nested(
    token_list, replace=True, op=Token.XOR, next_op=Token.XOR, removed=0
):
    """
    op must be binary
    Returns:
        Tuple of (Rewritten LTLFormula, remaining token list, # of subexpressions found)
    """
    if len(token_list) == 0:
        raise ParseError("Attempt to parse from empty token list")
    num_children, type_, *name = token_list.pop(0)
    if num_children == 2:
        lchild, token_list, removed = detect_pattern(
            token_list, replace, op, next_op, removed
        )
        rchild, token_list, removed = detect_pattern(
            token_list, replace, op, next_op, removed
        )
        if type_ is op and (lchild.type_ is next_op or rchild.type_ is next_op):
            return (
                LTLFormulaBinaryOp(type_, lchild, rchild),
                token_list,
                removed + 1,
            )
        else:
            return LTLFormulaBinaryOp(type_, lchild, rchild), token_list, removed
    elif num_children == 1:
        child, token_list, removed = detect_pattern(
            token_list, replace, op, next_op, removed
        )
        return LTLFormulaUnaryOp(type_, child), token_list, removed
    elif num_children == 0:
        if type_ == Token.AP:
            return LTLFormulaLeaf(type_, ap=name[0]), token_list, removed
        else:
            return LTLFormulaLeaf(type_, ap=None), token_list, removed
    else:
        raise ParseError("Illegal token '" + str(type_) + "'")


def detect_pattern(
    token_list, replace=True, op=Token.AND, next_op=Token.NOT, removed=0
):
    """
    op must be binary
    Returns:
        Tuple of (Rewritten LTLFormula, remaining token list, # of subexpressions found)
    """
    if len(token_list) == 0:
        raise ParseError("Attempt to parse from empty token list")
    num_children, type_, *name = token_list.pop(0)
    if num_children == 2:
        lchild, token_list, removed = detect_pattern(
            token_list, replace, op, next_op, removed
        )
        rchild, token_list, removed = detect_pattern(
            token_list, replace, op, next_op, removed
        )
        if type_ is op and (lchild.type_ is next_op or rchild.type_ is next_op):
            return (
                LTLFormulaBinaryOp(type_, lchild, rchild),
                token_list,
                removed + 1,
            )
        else:
            return LTLFormulaBinaryOp(type_, lchild, rchild), token_list, removed
    elif num_children == 1:
        child, token_list, removed = detect_pattern(
            token_list, replace, op, next_op, removed
        )
        return LTLFormulaUnaryOp(type_, child), token_list, removed
    elif num_children == 0:
        if type_ == Token.AP:
            return LTLFormulaLeaf(type_, ap=name[0]), token_list, removed
        else:
            return LTLFormulaLeaf(type_, ap=None), token_list, removed
    else:
        raise ParseError("Illegal token '" + str(type_) + "'")


def contains_no_biimplication_and(line):
    tokens = tokenize_formula(line.replace(" ", "").strip(), "network")
    _, _, amt = detect_pattern(tokens, op=Token.EQUIV, next_op=Token.AND)
    return amt == 0


def contains_no_biimplication_not(line):
    tokens = tokenize_formula(line.replace(" ", "").strip(), "network")
    _, _, amt = detect_pattern(tokens, op=Token.EQUIV, next_op=Token.NOT)
    return amt == 0


def contains_no_and_not(line):
    tokens = tokenize_formula(line.replace(" ", "").strip(), "network")
    _, _, amt = detect_pattern(tokens, op=Token.AND, next_op=Token.NOT)
    return amt == 0


def contains_no_and_xor(line):
    tokens = tokenize_formula(line.replace(" ", "").strip(), "network")
    _, _, amt = detect_pattern(tokens, op=Token.AND, next_op=Token.XOR)
    return amt == 0


def contains_no_not_b(line):
    return "! b" not in line


def contains_no_leftnegated(line):
    return "& !" not in line


def randomly(line):
    return random.choice([False, True])


def length_is_good(line):
    return len(line.split()) < 36


def remover(filename: str, addition: str, keep_function):
    with open(os.path.join(FOLDER_NAME, filename + ".src"), "r") as f:
        src_lines = [s.strip() for s in f.readlines()]

    # .removesuffix("_without_not_and_not")
    tgt_file = os.path.join(FOLDER_NAME, filename + ".tgt")
    if not os.path.exists(tgt_file):
        tgt_file = os.path.join(FOLDER_NAME, filename.split("_")[0] + ".tgt")
        print("using", tgt_file)
    with open(tgt_file, "r") as f:
        tgt_lines = [s.strip() for s in f.readlines()]

    new_src_lines = []
    new_tgt_lines = []
    for i, line in tqdm(enumerate(src_lines), total=len(src_lines)):
        if keep_function(line):
            new_src_lines.append(line)
            new_tgt_lines.append(tgt_lines[i])

    out_src_filename = os.path.join(FOLDER_NAME, filename + f"_{addition}.src")
    out_tgt_filename = os.path.join(FOLDER_NAME, filename + f"_{addition}.tgt")

    if os.path.exists(out_src_filename):
        raise FileExistsError()

    if os.path.exists(out_tgt_filename):
        raise FileExistsError()

    with open(out_src_filename, "w") as f:
        for line in new_src_lines:
            f.write(line + "\n")

    with open(out_tgt_filename, "w") as f:
        for line in new_tgt_lines:
            f.write(line + "\n")
    print("Wrote", len(new_src_lines) / len(src_lines), "%")


if __name__ == "__main__":
    # To remove patterns <->
    for s in ["train", "val", "test"]:
        remover(s, "without_biimplication_not", contains_no_biimplication_not)
        remover(s, "without_biimplication_and", contains_no_biimplication_and)
